#include <stdlib.h>
#include "SDL.h"
#include <stdio.h>

int main(int argc, char* args[])
{
    SDL_Window* gameWindow = nullptr;
    SDL_Renderer* gameRenderer = nullptr;

    int myInteger = 6;
    printf("%d\n", myInteger);
    printf("%u\n", &myInteger);

    int* myPointer;
    myPointer = (&myInteger);
    printf("%u\n", myPointer);
    printf("%d\n", *myPointer);

    SDL_Init(SDL_INIT_EVERYTHING);
    gameWindow = SDL_CreateWindow("Hello CIS4008",
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        800, 600,
        SDL_WINDOW_SHOWN);
    gameRenderer = SDL_CreateRenderer(gameWindow, 0, 0);

    SDL_SetRenderDrawColor(gameRenderer, 0, 0, 0, 255);

    SDL_RenderClear(gameRenderer);

    SDL_RenderPresent(gameRenderer);

    SDL_Delay(10000);


    SDL_DestroyRenderer(gameRenderer);
    SDL_DestroyWindow(gameWindow);

    SDL_Quit();

    exit(0);
}